import React from 'react';
import {GridColumnMenuContainer, GridFilterMenuItem, HideGridColMenuItem} from '@mui/x-data-grid';

const CustomColumnMenu = ({ hideMenu, currentColumn, open }) => {
    return (
        <GridColumnMenuContainer hideMenu={hideMenu} currentColumn={currentColumn} open={open}>
            <GridFilterMenuItem onClick={hideMenu} column={currentColumn} />
            <HideGridColMenuItem column={currentColumn} onClick={hideMenu} />
        </GridColumnMenuContainer>
    );
}

export default CustomColumnMenu;